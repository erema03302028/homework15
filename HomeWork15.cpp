﻿#include <iostream>

void print_numbers(bool odd, int N)
{
	for (int i = int(odd); i <= N; i += 2) 
	{
		std::cout << i << '\n';
	}	
}



int main()
{
	const int N = 20;
	print_numbers(0, N);
}
